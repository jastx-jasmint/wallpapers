# wallpapers

All artworks present in this repository are under the CC Licenses (CC BY-NC-ND 4.0).
If you have any questions, concerns or doubts, just ask. :)

You can find me via the email on my profile, on Matrix as jasmineteax, and Discord as jastx. I am also on the Garuda forums under the name lum1nuss.